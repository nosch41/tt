import React, { useState, useEffect, useRef } from "react";

export default function Acc() {
  const [history, setHistory] = useState(false);
  const count = useRef(0);
  // const interval = useRef(0);

  // const [motionData, setMotionData] = useState({
  //   acceleration: {
  //     x: 0,
  //     y: 0,
  //     z: 0,
  //   },
  //   accelerationIncludingGravity: {
  //     x: 0,
  //     y: 0,
  //     z: 0,
  //   },
  //   interval: 0,
  //   rotationRate: {
  //     alpha: 0,
  //     beta: 0,
  //     gamma: 0,
  //   },
  // });

  useEffect(() => {
    const arr = [];
    const handleDeviceMotion = (event) => {
      // count.current++;
      const {
        acceleration,
        accelerationIncludingGravity,
        interval,
        rotationRate,
      } = event;
      arr.unshift({
        timestamp: Date.now(),
        acceleration,
        accelerationIncludingGravity,
        interval,
        rotationRate,
      });
    };

    if (!history) {
      window.addEventListener("devicemotion", handleDeviceMotion, true);
      setTimeout(() => {
        window.removeEventListener("devicemotion", handleDeviceMotion, true);
        setHistory(arr);
      }, 3000);
    }
    return () => {
      window.removeEventListener("devicemotion", handleDeviceMotion, true);
    };
  });

  // useEffect(() => {
  //   const handleDeviceMotion = (e) => {
  //     const { x, y, z } = e.acceleration;
  //     // history.current.unshift({ x, y, z });
  //     count.current++;
  //     interval.current = e.interval;
  //     setData({ x, y, z });
  //   };
  //   console.log("test");
  //   window.addEventListener("devicemotion", handleDeviceMotion);
  //   return window.removeEventListener("devicemotion", handleDeviceMotion);
  // }, [data]);

  console.log("render");
  return (
    <>
      <p>
        Accelerometer (in m/s<sup>2</sup>)
      </p>
      {/*<p>Number of datapoints: {count.current}</p>*/}
      {history ? (
        <>
          <p>
            These are the {history.length} latest entries of the accelerometer
            sensor
          </p>
          <p>Data interval: ca. {history.find((h) => h)?.interval} ms</p>
          {history.map((h, i) => (
            <div key={i}>
              <p>
                Timestamp: {h?.timestamp} ms
                <br />
                x: {h?.acceleration?.x?.toFixed(10)} m/s<sup>2</sup>
                <br />
                y: {h?.acceleration?.y?.toFixed(10)} m/s<sup>2</sup>
                <br />
                z: {h?.acceleration?.z?.toFixed(10)} m/s<sup>2</sup>
                <br />
              </p>
            </div>
          ))}
        </>
      ) : (
        <>Shake your device for 3 seconds...</>
      )}
      {/*<p>History:</p>*/}
      {/*{history.current.map((d) => (*/}
      {/*  <>*/}
      {/*    <p>*/}
      {/*      x: {d.x}*/}
      {/*      <br />*/}
      {/*      y: {d.y}*/}
      {/*      <br />*/}
      {/*      z: {d.z}*/}
      {/*    </p>*/}
      {/*  </>*/}
      {/*))}*/}
    </>
  );
}
