import numpy as np
from scipy import stats
import pandas as pd
pd.set_option('display.max_columns', None)

from files import *
from preprocessing import *
from grouping import *

from plotting import *

import matplotlib.pyplot as plt
import numpy as np

from rich import print
from rich.table import Table
from rich.panel import Panel
from rich.columns import Columns
from rich.console import Group

from statsmodels.stats.anova import AnovaRM

DATA_FOLDER_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")

def load_data():
    df = load_files(DATA_FOLDER_PATH)

    total = []
    for index, data in df.iterrows():
        for result in data.results:
            acc = result.pop("acc")
            typing = result.pop("typing")
            errors, phraseLength = process_phrases(typing["providedPhrases"], typing["typedPhrases"])

            result["typing.errors"] = errors
            result["typing.corrections"] = typing["numberOfBackspaces"]
            result["typing.phraseLength"] = phraseLength
            result["typing.speed"] = typing["numberOfCharacters"] / typing["totalTime"] * 1000 # [char/sec]
            result["typing.tlx"] = sum(result.pop("tlx")) * 5 / 6
            result["typing.acc"] = concat_acc(acc, typing["pressTimestamps"])["mean"]

        results = pd.DataFrame(data.pop("results")).set_index("difficulty").sort_index()

        total.append(results)
    return total

def process(data: pd.DataFrame):
    processed_data = pd.DataFrame({"difficulty": [0,1,2,3]})
    for category in data:
        tmp = data[category].to_numpy().copy()
        for i in range(len(tmp) - 1, -1, -1):
            tmp[i] = np.abs(tmp[i] - (tmp[0] if i > 0 else tmp[i]))

        processed_data[category] = tmp

    return processed_data.set_index("difficulty").sort_index()

def process_data(absolute_data: list[pd.DataFrame]) -> list[pd.DataFrame]:
    return [process(item) for item in absolute_data]

def group_by_category(data: list[pd.DataFrame]):
    group = dict()
    for item in data:
        for category in item:
            if category not in group:
                group[category] = pd.DataFrame()

            participant = dict()
            for (difficulty, value) in enumerate(item[category]):
                participant[difficulty] = value
            group[category] = pd.concat([group[category], pd.DataFrame(participant, index=[0])]).reset_index(drop=True)
    return group

def shapiro_test(grouped_absolute_data):
    table = Table(title="Shapiro-Wilk Test")

    table.add_column("key", style="cyan", no_wrap=True)
    table.add_column("p-value", style="magenta")

    for category in grouped_absolute_data:
        res = stats.shapiro(grouped_absolute_data[category][0])
        table.add_row(category, str(res.statistic))
    
    print(Panel(table, title="Shapiro Baseline"))

def repeated_anova(data, depvar, start=0):
    adapted_data = pd.DataFrame()
    for index, row in data.iterrows():
        for (difficulty, value) in enumerate(row[start:]):
            new_row = pd.DataFrame({"participant": index, "difficulty": difficulty, depvar: value}, index=[0])
            adapted_data = pd.concat([adapted_data, new_row]).reset_index(drop=True)

    return AnovaRM(data=adapted_data, depvar=depvar, subject='participant', within=['difficulty']).fit()

def pair_ttest(data1, data2):
    return stats.ttest_rel(data1, data2).pvalue

def print_anova(*data_title_pair, title):
    groups = list()
    for pair in data_title_pair:
        groups.append(Group(pair[0], str(pair[1])))
    print(Panel(Columns(groups), title=title))


def test_h1(grouped_relative_data):
    txl = grouped_relative_data["typing.tlx"]
    r1 = repeated_anova(txl, "TLX")
    r2 = repeated_anova(txl, "TLX", 1)
    ttest12 = pair_ttest(txl[1], txl[2])
    ttest23 = pair_ttest(txl[2], txl[3])
    print_anova(("ANOVA all levels", r1), ("ANOVA only n-back levels", r2), ("Pair t-Test: 1-2", ttest12), ("Pair t-test: 2-3", ttest23), title="TLX - H1")
    pass

def test_h2(grouped_relative_data):
    speed = grouped_relative_data["typing.speed"]
    r1 = repeated_anova(speed, "Speed")
    r2 = repeated_anova(speed, "Speed", 1)
    ttest12 = pair_ttest(speed[1], speed[2])
    ttest23 = pair_ttest(speed[2], speed[3])
    print_anova(("ANOVA: all levels", r1), ("ANOVA: only n-back levels", r2), ("Pair t-Test: 1-2", ttest12), ("Pair t-test: 2-3", ttest23), title="Speed - H2")
    pass

def test_h3(grouped_relative_data):
    errors = grouped_relative_data["typing.errors"]
    r1 = repeated_anova(errors, "Errors")
    r2 = repeated_anova(errors, "Errors", 1)
    print_anova(("ANOVA all levels", r1), ("ANOVA only n-back levels", r2), title="Errors - H3")
    pass

def test_h4(grouped_relative_data):
    corrections = grouped_relative_data["typing.corrections"]
    r1 = repeated_anova(corrections, "Corrections")
    r2 = repeated_anova(corrections, "Corrections", 1)
    print_anova(("ANOVA all levels", r1), ("ANOVA only n-back levels", r2), title="Corrections - H4")
    pass

def test_h5(grouped_relative_data):
    acc = grouped_relative_data["typing.acc"]
    r1 = repeated_anova(acc, "Accelerometer")
    r2 = repeated_anova(acc, "Accelerometer", 1)
    print_anova(("ANOVA all levels", r1), ("ANOVA only n-back levels", r2), title="Accelerometer - H5")
    pass

def linear_fit(data):
    # Generate data
    x = list()
    y = list()
    for column in data["typing.tlx"].columns:
        x += data["typing.tlx"][column].tolist()
    for column in data["typing.speed"].columns:
        y += data["typing.speed"][column].tolist()

    # Initialize layout
    fig, ax = plt.subplots(figsize = (9, 9))

    # Add scatterplot
    ax.scatter(x, y, s=60, alpha=0.7, edgecolors="k")

    # Fit linear regression via least squares with numpy.polyfit
    # It returns an slope (b) and intercept (a)
    # deg=1 means linear fit (i.e. polynomial of degree 1)
    b, a = np.polyfit(x, y, deg=1)

    # Create sequence of 100 numbers from 0 to 100 
    xseq = np.linspace(0, 100, num=100)

    # Plot regression line
    ax.plot(xseq, a + b * xseq, color="k", lw=2.5)
    fig.show()
    pass

def significance_tests():
    absolute_data = load_data()
    relative_data = process_data(absolute_data)

    grouped_absolute_data = group_by_category(absolute_data)
    grouped_relative_data = group_by_category(relative_data)

    linear_fit(grouped_absolute_data)

    shapiro_test(grouped_absolute_data)
    # -> Daten sind parametrisch

    test_h1(grouped_relative_data)
    test_h2(grouped_relative_data)
    test_h3(grouped_relative_data)
    test_h4(grouped_relative_data)
    test_h5(grouped_relative_data)
    pass

significance_tests()