import json
import os
import pandas as pd

from preprocessing import *

def load_files(data_folder_path):
    dataframes = []
    for filename in os.listdir(data_folder_path):
        file_path = os.path.join(data_folder_path, filename)
        if not file_path.endswith(".json"):
            continue

        with open(file_path, 'r') as file:
            data = json.load(file)
            df_part = pd.json_normalize(data)
            dataframes.append(df_part)
    return pd.concat(dataframes, ignore_index=True)
